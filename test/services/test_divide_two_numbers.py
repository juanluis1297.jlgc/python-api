from src.services.divide_two_numbers import divide_two_numbers


def test_possitive_numbers():
  factor_1, factor_2 = 4, 2
  res = divide_two_numbers(factor_1, factor_2)
  assert res == 2
